<!--============================= TITLE =============================-->

<head>
    <style>
.sliderhome {
   width: 100%;
}
    </style>
    <title><?php echo $title; ?></title>
</head>
<!--============================= END =============================-->

<!--============================= SLIDER =============================-->
<section>

</div>
    <div class="slider_img layout_two">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
                <li data-target="#carousel" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                     <img style="filter:brightness(100%)" class="d-block sliderhome" src="<?php echo base_url() .
                                                                                                                'theme/images/slider/banner9.png'; ?>" alt="First slide">
                    <!--<img style="filter:brightness(100%)" class="d-block sliderhome" src="<?php echo base_url() .
                                                                                                                'theme/images/slider/banner5.png'; ?>" alt="First slide">-->
                    <!--<div class="carousel-caption d-md-block background">-->
                    <!--    <div class="slider_title transbox">-->
                    <!--        <h1><strong>Simpel &amp; Praktis</strong></h1>-->
                    <!--        <h4 style="color: white; font-size:24px;">Training menggunakan pendekatan learning by doing<br> menerapkan cara-cara praktis dan simpel</h4>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
                <!--<div class="carousel-item">
                    <img style="filter:brightness(100%)" class="d-block sliderhome" src="<?php echo base_url() .
                                                                                                                'theme/images/slider/banner6.png'; ?>" alt="Second slide">-->
                    <!--<div class="carousel-caption d-md-block">-->
                    <!--    <div class="slider_title">-->
                    <!--        <h1><strong>Trainer Berkomitmen Tinggi</strong></h1>-->
                    <!--        <h4 style="color: white; font-size:24px;">Memiliki pengalaman terbaik dalam proses penulisan artikel terindeks Scopus</h4>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
                <!--<div class="carousel-item">
                    <img style="filter:brightness(100%)" class="d-block" src="<?php echo base_url() .
                                                                                                                'theme/images/slider/banner7.png'; ?>" alt="Third slide">-->
                    <!--<div class="carousel-caption d-md-block">-->
                    <!--    <div class="slider_title">-->
                    <!--        <h1><strong>Learning by Objective</strong></h1>-->
                    <!--        <h4 style="color: white; font-size:24px;">Training memiliki capaian akhir yang jelas dan terukur.<br> Setiap sesi pertemuan peserta dipacu untuk mencapai target yang jelas</h4>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
                <!--<div class="carousel-item">
                    <img style="filter:brightness(100%)" class="d-block" src="<?php echo base_url() .
                                                                                                                'theme/images/slider/banner8.png'; ?>" alt="Fourth slide">-->
                    <!--<div class="carousel-caption d-md-block">-->
                    <!--    <div class="slider_title">-->
                    <!--        <h1><strong>Kecepatan dan Digital Tools</strong></h1>-->
                    <!--        <h4 style="color: white; font-size:24px;">Mengacu pada norma milenial yaitu speed.<br> Membiasakan penggunaan alat-alat digital </h4>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
            </div>
            <!--<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">-->
            <!--    <i class="icon-arrow-left fa-slider" aria-hidden="true"></i>-->
            <!--    <span class="sr-only">Previous</span>-->
            <!--</a>-->
            <!--<a class="carousel-control-next" href="#carousel" role="button" data-slide="next">-->
            <!--    <i class="icon-arrow-right fa-slider" aria-hidden="true"></i>-->
            <!--    <span class="sr-only">Next</span>-->
            <!--</a>-->
        </div>
    </div>
</section>
<!--============================= END =============================-->

<!--============================= ABOUT =============================-->
<!--<section class="welcome_about">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2 >Tentang Kami</h2>
                <hr size="6px" width="38%" align="left" color="orange" style="margin-top: -15px;">
                <p>Rumah Scopus (RSc) adalah penyedia layanan jasa di bidang pendampingan penyusunan artikel ilmiah dan publikasi ke jurnal terindeks Scopus.
                    Pendampingan dilaksanakan secara online dan offline secara personal dan kelompok atau institusi. Rumah Scopus berdiri pada 20 Februari 2019 yang berpusat di Yogyakarta.
                    RSc hadir atas kegelisahan tentang publikasi jurnal terindeks Scopus. Strategi utama RSc menggabungkan teknologi, ilmu pengetahuan dan pengalaman dalam menyusun
                    artikel sesuai standar Internasional. </p>

            </div>
            <div class="col-md-5 mt-5">
                <img src="<?php echo base_url() .
                                'theme/images/logo/logo.png'; ?>">
            </div>
        </div>
    </div>
</section>-->


<!--<section class="clearfix about about-style2">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Selamat Datang</h2>
                <p>Rumah Scopus (RSc) adalah penyedia layanan jasa di bidang pendampingan penyusunan artikel ilmiah dan publikasi ke jurnal terindeks Scopus.
                    Pendampingan dilaksanakan secara online dan offline secara personal dan kelompok atau institusi. Rumah Scopus berdiri pada 20 Februari 2019 yang berpusat di Yogyakarta.
                    RSc hadir atas kegelisahan tentang publikasi jurnal terindeks Scopus. Strategi utama RSc menggabungkan teknologi, ilmu pengetahuan dan pengalaman dalam menyusun
                    artikel sesuai standar Internasional. </p>

            </div>
            <div class="col-md-4">
                <img  src="<?php echo base_url() .
                                'theme/images/logo/scopus1.png'; ?>" class="img-fluid about-img" alt="#">
            </div>
        </div>
    </div>
</section>-->
<!--============================= END ABOUT =============================-->

<!--============================= LAYANAN KAMI =============================-->
<section class="our_courses" style="background-color: #F5FAFF; margin-top:-50px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" style="position: relative;">
                <h2>Layanan Kami</h2>
                <hr size="6px" width="100%" align="left" color="orange" style="margin-top: -15px;">
            </div>
        </div>
        <div class="row">
        
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card courses_box mb-4" style="border: 2px solid orange;">
                    <div class="card-header text-center" style="background-color: orange; color:white;">
                        <b>Produk Terbaru!</b>
                    </div>
                    <img style="background-color: rgba(22, 160, 133, 0.1); " src="<?php echo base_url() . 'theme/images/asset/webinarrmv.png'; ?>">
                    <div class="card-body">
                        <center>
                            <h5 class="card-title mt-3"><b>Free Webinar Blended</b></h5>
                        </center>
                        <hr size="6px" width="50%" align="center" color="orange">
                        <p class="mb-3" style="margin-left:15px; margin-right:15px;">Pelayanan kegiatan seminar yang dilakukan secara online (zoom meet) dan offline (Joglo Rumah Scopus Yogyakarta) setiap hari rabu malam, yang akan di dampingi langsung oleh Dr. Jumintono Suwardi Joyo Sumarto, M.Pd.</p>
                        <center>
                            <a href="https://api.whatsapp.com/send?phone=+6281226883280&text=Hallo%20kak!,%20Saya%20ingin%20informasi%20terkait%20program%20Scopus%20Camp%20di%20Rumah%20Scopus." class="btn btn-warning mb-3 mt-3" style="color:white; border-radius:30px; background-color:orange;">
                                Daftar Sekarang!
                            </a>
                        </center>
                        <!--<div class="mt-2 mb-5" style="margin-left:15px; margin-right:15px;">
                            <details>
                                <p>&#10004; Secara Offline atau tatap muka</p>
                                <p>&#10004; Pelaksanaan selama 3 hari</p>
                                <p>&#10004; Bertempat di Rumah Scopus Pusat atau Cabang Rumah Scopus</p>
                                <p>&#10004; DiDampingi Trainer Profesionel</p>
                                <p>&#10004; Harga Terjangkau</p>
                            </details>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card courses_box mb-4" style="border: 2px solid orange;">
                    <div class="card-header text-center" style="background-color: orange; color:white;">
                        <b>Penawaran Terbaik!</b>
                    </div>
                    <img style="background-color:  rgba(213, 15, 37, 0.1)" src="<?php echo base_url() . 'theme/images/asset/camprmv.png'; ?>">
                    <div class="card-body">
                        <center>
                            <h5 class="card-title mt-3"><b>Scopus Camp</b></h5>
                        </center>
                        <hr size="6px" width="50%" align="center" color="orange">
                        <p class="mb-3" style="margin-left:15px; margin-right:15px;">Memberikan pelatihan penulisan paper yang berjalan selama 3 hari secara offline atau tatap muka. Pada Scopus Camp ini bertempat di Rumah Scopus pusat yang berada di Turi Sleman dan Cabang Rumah Scopus, dengan Trainer yang sudah Profesional</p>
                        <center>
                            <a href="https://api.whatsapp.com/send?phone=+6281226883280&text=Hallo%20kak!,%20Saya%20ingin%20informasi%20terkait%20program%20Scopus%20Camp%20di%20Rumah%20Scopus." class="btn btn-warning mb-3 mt-3" style="color:white; border-radius:30px; background-color:orange;">
                                Daftar Sekarang!
                            </a>
                        </center>
                        <!--<div class="mt-2 mb-5" style="margin-left:15px; margin-right:15px;">
                            <details>
                                <p>&#10004; Secara Offline atau tatap muka</p>
                                <p>&#10004; Pelaksanaan selama 3 hari</p>
                                <p>&#10004; Bertempat di Rumah Scopus Pusat atau Cabang Rumah Scopus</p>
                                <p>&#10004; DiDampingi Trainer Profesionel</p>
                                <p>&#10004; Harga Terjangkau</p>
                            </details>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card courses_box mb-4 mt-5">
                    <img style="background-color: rgba(22, 160, 133, 0.1); " src="<?php echo base_url() . 'theme/images/asset/webinarrmv.png'; ?>">
                    <div class="card-body">
                        <center>
                            <h5 class="card-title mt-3"><b>Webinar Scopus Training</b></h5>
                        </center>
                        <hr size="6px" width="50%" align="center" color="orange">
                        <p class="mb-3" style="margin-left:15px; margin-right:15px;">Pelayanan kegiatan seminar yang dilakukan secara online dengan menggunakan media zoom meet, yang dilaksanakan selama 10 kali sesi pertemuan dengan di dampingi oleh Trainer Profesionel dan Dr. Jumintono Suwardi Joyo Sumarto, M.Pd</p>
                        <center>
                            <a href="https://api.whatsapp.com/send?phone=+6281226883280&text=Hallo%20kak!,%20Saya%20ingin%20informasi%20terkait%20program%20Webinar%20Scopus%20Training%20di%20Rumah%20Scopus." class="btn btn-warning mb-3 mt-3" style="color:white; border-radius:30px; background-color:orange;">
                                Daftar Sekarang!
                            </a>
                        </center>
                        <!--<div class="mt-2 mb-5" style="margin-left:15px; margin-right:15px;">
                            <details>
                                <p>&#10004; Secara Online</p>
                                <p>&#10004; Menggunakan Media Zoom Meet</p>
                                <p>&#10004; 10x sesi pertemuan</p>
                                <p>&#10004; DiDampingi Trainer Profesionel</p>
                                <p>&#10004; DiDampingi oleh Presiden Rumah Scopus</p>
                                <p>&#10004; Harga Terjangkau</p>
                            </details>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card courses_box mb-4 mt-5">
                    <img style="background-color:  rgba(250, 188, 9, 0.1)" src="<?php echo base_url() . 'theme/images/asset/pngbrmv.png'; ?>">
                    <div class="card-body">
                        <center>
                            <h5 class="card-title mt-3"><b>Program Nyata Percepatan Guru Besar (PNP GB)</b></h5>
                        </center>
                        <hr size="6px" width="50%" align="center" color="orange">
                        <p class="mb-2" style="margin-left:15px; margin-right:15px;">Memberikan layanan dalam penyusunan paper yang bereputasi internasional yang terindeks Scopus, dengan di dampingi oleh trainer profesional</p>
                        <center>
                            <a href="https://api.whatsapp.com/send?phone=+6281226883280&text=Hallo%20kak!,%20Saya%20ingin%20informasi%20terkait%20program%20Pendampingan%20Nyata%20Percepatan%20Guru%20Besar%20di%20Rumah%20Scopus." class="btn btn-warning mb-3 mt-3" style="color:white; border-radius:30px; background-color:orange;">
                                Daftar Sekarang!
                            </a>
                        </center>
                        <!--<div class="mt-2 mb-5" style="margin-left:15px; margin-right:15px;">
                            <details>
                                <p>&#10004; Secara Offline atau tatap muka</p>
                                <p>&#10004; Bertempat di Rumah Scopus Pusat</p>
                                <p>&#10004; DiDampingi Trainer Profesionel</p>
                                <p>&#10004; Harga Terjangkau</p>
                            </details>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card courses_box mb-4">
                <img style="background-color: rgba(121, 90, 71, 0.1)" src="<?php echo base_url() . 'theme/images/asset/workshoprmv.png'; ?>">
                    <div class="card-body">
                        <center>
                            <h5 class="card-title mt-3"><b>Workshop Penulisan Artikel Ilmiah</b></h5>
                        </center>
                        <hr size="6px" width="50%" align="center" color="orange">
                        <p style="margin-left:15px; margin-right:15px;">Penyampaian materi tentang penulisan artikel ilmian yang terindeks scopus, oleh para trainer berpengalaman dan profesional dalam penyusunan paper yang terindeks scopus </p>
                        <center>
                            <a href="https://api.whatsapp.com/send?phone=+6281226883280&text=Hallo%20kak!,%20Saya%20ingin%20informasi%20terkait%20program%20Multi%20Entri%20Multi%20Exit%20di%20Rumah%20Scopus." class="btn btn-warning mb-3 mt-3" style="color:white; border-radius:30px; background-color:orange;">
                                Pesan Sekarang!
                            </a>
                        </center>
                        <!--<div class="mt-2 mb-5" style="margin-left:15px; margin-right:15px;">
                            <details>
                                <p>&#10004; Secara Offline atau tatap muka</p>
                                <p>&#10004; Bertempat di Rumah Scopus Pusat</p>
                                <p>&#10004; Waktu yang fleksibel</p>
                                <p>&#10004; DiDampingi Trainer Profesional</p>
                                <p>&#10004; Harga Terjangkau</p>
                            </details>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card courses_box mb-4">
                    <img style="background-color:rgba(187, 120, 36, 0.1)" src="<?php echo base_url() .
                                                                                    'theme/images/logo/waktu.png'; ?>">
                    <div class="card-body">
                        <center>
                            <h5 class="card-title mt-3"><b>Multi Entri Multi Exit (MeMe)</b></h5>
                        </center>
                        <hr size="6px" width="50%" align="center" color="orange">
                        <p style="margin-left:15px; margin-right:15px;">memberikan layanan dalam penyusunan paper, dengan waktu yang relatif fleksibel atau menyesuaikan, oleh para trainer yang profesional dan berpengalam dalam penyusunan paper terindeks scopus</p>
                        <center>
                            <a href="https://api.whatsapp.com/send?phone=+6281226883280&text=Hallo%20kak!,%20Saya%20ingin%20informasi%20terkait%20program%20Multi%20Entri%20Multi%20Exit%20di%20Rumah%20Scopus." class="btn btn-warning mb-3 mt-3" style="color:white; border-radius:30px; background-color:orange;">
                                Pesan Sekarang!
                            </a>
                        </center>
                        <!--<div class="mt-2 mb-5" style="margin-left:15px; margin-right:15px;">
                            <details>
                                <p>&#10004; Secara Offline atau tatap muka</p>
                                <p>&#10004; Bertempat di Rumah Scopus Pusat</p>
                                <p>&#10004; Waktu yang fleksibel</p>
                                <p>&#10004; DiDampingi Trainer Profesional</p>
                                <p>&#10004; Harga Terjangkau</p>
                            </details>
                        </div>-->
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</section>
<!--============================= END =============================-->

<!--============================= MENGAPA MEMILIH KAMI =============================-->
<section class="digital-marketing-service mt-5 mb-5" id="digital-marketing-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-7 grid-margin grid-margin-lg-0">
                <h3 class="m-0 card-title" style="font-size: 30px;"><b>Trainer Profesional</b></h3>
                <div class="col-lg-10 col-xl-10 p-0">
                    <p class="py-4 m-0 text-muted">
                        Rumah Scopus Foundation memiliki Trainer yang sudah berpengalaman dan profesional menulis paper. Trainer kami akan membantu anda untuk
                        menyelesaikan paper sampai submit dan publish hingga terindeks Scopus.
                    </p>
                    <!--<p class="font-weight-medium text-muted"></p>-->
                </div>
            </div>
            <div class="col-12 col-lg-5 p-0 img-digital grid-margin grid-margin-lg-0">
                <img src="<?php echo base_url() .
                                'theme/images/trainer.png'; ?>" alt="" class="img-fluid">
            </div>
        </div>

        <div class="row align-items-center mt-5">
            <div class="col-12 col-lg-5 p-0 text-center flex-item grid-margin">
                <img src="<?php echo base_url() .
                                'theme/images/biaya.png'; ?>" alt="" class="img-fluid">
            </div>
            <div class="col-12 col-lg-6 flex-item grid-margin">
                <h3 class="m-0 card-title" style="font-size: 30px;"><b>Biaya Terjangkau</b></h3>
                <div class="col-lg-12 col-xl-12 p-0">
                    <p class="py-4 m-0 text-muted">
                        Dengan harga yang terjangkau anda dapat mengikuti program yang diberikan oleh Rumah Scopus tanpa menguras kantong. Meskipun harga terjangkau,
                        tetapi kualitas kami utamakan baik pelayanan maupun fasilitas yang kami berikan kepada anda.
                    </p>
                </div>
            </div>
        </div>

        <div class="row align-items-center mt-5">
            <div class="col-12 col-lg-7 grid-margin grid-margin-lg-0">
                <h3 class="m-0 card-title" style="font-size: 30px;"><b>Fasilitas Lengkap</b></h3>
                <div class="col-lg-10 col-xl-10 p-0">
                    <p class="py-4 m-0 text-muted">
                        Rumah Scopus Foundation memiliki fasilitas yang lengkap baik dari segi internet, tempat tinggal, makanan ala kampung, suasana sejuk alami yang dapat memenuhi mendukung kenyamanan dalam menyusun paper. Kami juga menyediakan kolam renang
                        yang dapat anda gunakan selama berada di lingkungan Rumah Scopus.
                    </p>
                    <!--<p class="font-weight-medium text-muted"></p>-->
                </div>
            </div>
            <div class="col-12 col-lg-5 p-0 img-digital grid-margin grid-margin-lg-0">
                <img src="<?php echo base_url() .
                                'theme/images/fasilitas.png'; ?>" alt="" class="img-fluid">
            </div>
        </div>

    </div>
</section>
<!--============================= END =============================-->

<!--============================= AGENDA  & ARTIKEL =============================-->
<section class="event" style="margin-top: -80px;">
    <div class="container">
        <div class="row">

            <!-- ARTIKEL -->
            <div class="col-lg-6">
                <h2>Artikel Terbaru</h2>
                <hr size="6px" width="50%" align="left" color="orange" style="margin-top: -15px;">
                <div class="event-img2">
                    <?php foreach ($berita->result() as $row) : ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="<?php echo base_url() .
                                                'assets/images/' .
                                                $row->tulisan_gambar; ?>" class="img-fluid" alt="event-img">
                            </div>
                            <div class="col-sm-9 mt-2">
                                <h3>
                                    <?php echo limit_words($row->tulisan_judul, 5) .
                                        '...'; ?>
                                </h3>
                                <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $row->tanggal; ?>
                                <span>|</span>
                                <!--<i class="fa fa-user" aria-hidden="true"></i> <?php echo $row->tulisan_author; ?> -->
                                <i class="fa fa-tags" aria-hidden="true"></i> <?php echo $row->tulisan_kategori_nama; ?>
                                </span>
                                <!--<p><?php echo limit_words($row->tulisan_isi, 10) .
                                            '...'; ?></p>-->
                                <a href="<?php echo site_url(
                                                'artikel'
                                            ); ?>" class="btn btn-warning mt-2" style="color:white; border-radius:30px; background-color:orange; float:right;">
                                    Selengkapnya
                                </a>
                                <hr class="event_line">
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <!-- END -->

            <!-- AGENDA -->
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Agenda Terbaru</h2>
                        <hr size="6px" width="50%" align="left" color="orange" style="margin-top: -15px;">
                        <?php foreach ($agenda->result() as $row) : ?>
                            <div class="event_date">
                                <div class="event-date-wrap">
                                    <p><?php echo date(
                                            'd',
                                            strtotime($row->agenda_tanggal)
                                        ); ?></p>
                                    <span><?php echo date(
                                                'M. y',
                                                strtotime($row->agenda_tanggal)
                                            ); ?></span>
                                </div>
                            </div>
                            <div class="date-description">
                                <h3>
                                    <?php echo $row->agenda_nama; ?>
                                </h3>
                                <p>
                                    <i class="fa fa-calendar" aria-hidden="true"> <?php echo date(
                                                                                        'd M y',
                                                                                        strtotime($row->agenda_mulai)
                                                                                    ); ?> s/d <?php echo date(
                                                                                                    'd M y',
                                                                                                    strtotime($row->agenda_selesai)
                                                                                                ); ?></i>
                                    |
                                    <i class="fa fa-map-marker" aria-hidden="true"> <?php echo $row->agenda_tempat; ?></i>
                                    |
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-alarm" viewBox="0 0 16 16">
                                        <path d="M8.5 5.5a.5.5 0 0 0-1 0v3.362l-1.429 2.38a.5.5 0 1 0 .858.515l1.5-2.5A.5.5 0 0 0 8.5 9V5.5z" />
                                        <path d="M6.5 0a.5.5 0 0 0 0 1H7v1.07a7.001 7.001 0 0 0-3.273 12.474l-.602.602a.5.5 0 0 0 .707.708l.746-.746A6.97 6.97 0 0 0 8 16a6.97 6.97 0 0 0 3.422-.892l.746.746a.5.5 0 0 0 .707-.708l-.601-.602A7.001 7.001 0 0 0 9 2.07V1h.5a.5.5 0 0 0 0-1h-3zm1.038 3.018a6.093 6.093 0 0 1 .924 0 6 6 0 1 1-.924 0zM0 3.5c0 .753.333 1.429.86 1.887A8.035 8.035 0 0 1 4.387 1.86 2.5 2.5 0 0 0 0 3.5zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1z" />
                                    </svg> <?php echo $row->agenda_waktu; ?>
                                </p>
                                <p>
                                    <?php echo limit_words(
                                        $row->agenda_deskripsi,
                                        10
                                    ) . '...'; ?>
                                </p>
                                <a href="<?php echo site_url(
                                                'agenda'
                                            ); ?>" class="btn btn-warning" style="color:white; border-radius:30px; background-color:orange; margin-bottom:-30px; float:right;">
                                    Selengkapnya
                                </a>
                                <hr class="event_line">
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <!-- END -->

        </div>
    </div>
</section>
<!--============================= END =============================-->

<!--============================= ARTIKEL TERBARU =============================-->
<!--<section class="our_courses" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Artikel Terbaru</h2>
            </div>
        </div>
        <div class="row">
            <?php foreach ($berita->result() as $row) : ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="courses_box mb-4">
                        <div class="course-img-wrap">
                            <img src="<?php echo base_url() .
                                            'assets/images/' .
                                            $row->tulisan_gambar; ?>" class="img-fluid" alt="courses-img">
                        </div>
                        <a href="<?php echo site_url(
                                        'artikel/' . $row->tulisan_slug
                                    ); ?>" class="course-box-content">
                            <h3 style="text-align:center;"><?php echo limit_words(
                                                                $row->tulisan_judul,
                                                                5
                                                            ) . '...'; ?></h3>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div> <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="<?php echo site_url(
                                'artikel'
                            ); ?>" class="btn btn-default btn-courses">Selengkapnya</a>
            </div>
        </div>
    </div>
</section>-->
<!--============================= END =============================-->

<!--============================= VIDEO PROFILE =============================-->
<section id="call-to-action" class="call-to-action">
    <div class="container text-center">
        <a href="https://www.youtube.com/watch?v=LM4aySlE2-A" class="glightbox play-btn"></a>
        <h3>Profile Rumah Scopus Foundation</h3>
        <p>Video ini merupakan video profil resmi dari Rumah Scopus, yang dapat membantu anda untuk mengetahui layanan yang berada di Rumah Scopus. </p>
        <a class="cta-btn" href="https://api.whatsapp.com/send?phone=+6281226883280&text=Hallo%20kak!%20Saya%20mau%20tanya%20tentang%20layanan%20Rumah%20Scopus.">Hubungi Sekarang!</a>
    </div>
</section>
<!--============================= END =============================-->

<!--============================= BRAND PARTNER =============================-->
<!--<section class="our_courses" style=" margin-top:-50px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" style="position: relative;">
                <h2>Partner Kami</h2>
                <hr size="6px" width="100%" align="left" color="orange" style="margin-top: -15px;">
            </div>
        </div>
        <div class="row">
            <div class="brand-area pt-90">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="brand-logo d-flex align-items-center justify-content-center justify-content-md-between">
                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/undip.png'; ?>" alt="brand" />
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/unesa.png'; ?>" alt="brand" />
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/uins.png'; ?>" alt="brand" />
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.4s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/uns.png'; ?>" alt="brand" />
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/univpakuan.png'; ?>" alt="brand"/>
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/iain.png'; ?>" alt="brand"/>
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/poltekes.png'; ?>" alt="brand"/>
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/uad.png'; ?>" alt="brand"/>
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/umy.png'; ?>" alt="brand"/>
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/univborobudur.png'; ?>" alt="brand"/>
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/upn.png'; ?>" alt="brand"/>
                                </div>

                                <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                    <img src="<?php echo base_url() .
                                                    'theme/images/partner/ust.png'; ?>" alt="brand"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->

<!--<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" style="position: relative;">
                <h2>Partner Kami</h2>
                <hr size="6px" width="100%" align="left" color="orange" style="margin-top: -15px;">
            </div>
            <div class="col-md-12">
                <div class="single-item">

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/ust.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Sarjanawiyata Tamansiswa</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/undip.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Diponegoro Semarang</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/unesa.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Negeri Surabaya</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/uinsurakarta.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Islam Negeri Surakarta</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/unsmaret.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Sebelas Maret</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/unpak1.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Pakuan Bogor</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/iain1.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>IAIN Ponorogo</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/poltekes1.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Politeknik Kesehatan Semarang</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/uad1.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Ahmad Dahlan</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/umy1.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Muhammadiyah Yogyakarta</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/univborobudur1.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Borobudur Jakarta Timur</b></h5>
                    </div>

                    <div class="quote">
                        <center><img src="<?php echo base_url() .
                                                'theme/images/partner/upn1.png'; ?>" alt="brand" /></center>
                        <h5 class="card-title mt-3"><b>Universitas Pembangunan Nasional Veteran</b></h5>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>-->

<section id="clients" class="clients mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" style="position: relative;">
                <h2><b>Partner Kami</b></h2>
                <hr size="6px" width="100%" align="left" color="orange"">
            </div>
            <div class=" clients-slider swiper">
                <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/iain.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/poltekes.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/uad.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/uinsurakarta.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/umy.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/undip.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/unesa.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/unbor.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/unpak.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/uns.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/upn.png' ?>" class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="<?php echo base_url() .
                                                            'theme/images/partner/new/ust.png' ?>" class="img-fluid" alt=""></div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--============================= END =============================-->

<!--============================= VIDEO =============================-->
<section class="welcome_about" style="background-color: #F5FAFF;">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <blockquote> "Dengan Adanya Rumah Scopus saya menjadi terbantu dan menjadi terindeks scopus" </blockquote>
                <a href="https://drive.google.com/drive/folders/1As4Q5dAJ26jUpew_Kt4pkOtaOf8APKHK" class="btn btn-warning mb-3 mt-3" style="color:white; border-radius:30px; background-color:orange;">
                    Lihat Video <i class="fa fa-play" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-md-5">
                <img style="border-radius: 10%;" src="<?php echo base_url() .
                                                            'theme/images/joe.jpg'; ?>">
                <h4 align="center">Dr. Jumintono Suwardi Joyo Sumarto, M.Pd</h4>
            </div>
        </div>
    </div>
</section>
<!--============================= END =============================-->

<!--============================= INFO CHART =============================-->
<!--<div class="detailed_chart">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom">
                <div class="chart-img">
                    <img src="<?php echo base_url() .
                                    'theme/images/chart-icon_1.png'; ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_guru; ?></span>Trainer
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom chart_top">
                <div class="chart-img">
                    <img src="<?php echo base_url() .
                                    'theme/images/chart-icon_2.png'; ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_siswa; ?></span> Member
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_top">
                <div class="chart-img">
                    <img src="<?php echo base_url() .
                                    'theme/images/chart-icon_3.png'; ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter">11</span> Cabang
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="chart-img">
                    <img src="<?php echo base_url() .
                                    'theme/images/chart-icon_4.png'; ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_agenda; ?></span> Agenda</p>
                </div>
            </div>
        </div>
    </div>
</div>-->
<!--============================= END =============================-->

<!--============================= TESTIMONIAL =============================-->
<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Testimonial</h2>
                <hr size="50%" width="20%" align="center" color="orange" style="margin-top: -15px;">
            </div>
            <div class="col-md-12">
                <div class="single-item">
                    <div class="quote">
                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                        <p class="quote_text">Rumah Scopus benar-benar mengagumkan. Saya sangat senang bisa bergabung dengan Rumah Scopus.</p>
                        <div class="testi-img_block">
                            <!--<img src="<?php echo base_url() .
                                                'theme/images/student-1.png'; ?>" class="img-fluid" alt="#">-->
                            <p><span></span></p>
                        </div>
                    </div>
                    <div class="quote">
                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                        <p class="quote_text">Rumah Scopus sangat membatu saya, hingga jurnal saya bisa go internasional. </p>
                        <div class="testi-img_block">
                            <!--<img src="<?php echo base_url() .
                                                'theme/images/student-2.png'; ?>" class="img-fluid" alt="#">-->
                            <p><span></span></p>
                        </div>
                    </div>
                    <div class="quote">
                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                        <p class="quote_text">Hanya bermodalkan Free Webiner dan dilanjutkan dengan Pelatihan Batch 52 online, saya berhasil menulis di Jurnal Internasional terindeks Scopus Q1 = 1 artikel (Accepted), Q2 = 1 artikel (on Review), dan Q3 = 1 artikel (Accepted). Dan semoga terus berlanjut menghasilkan karya berikutnya. Terima kasih rumah scopus, ilmunya sangat sangattttttt bermanfaat.....🙏🔥🔥🔥 Semoga kapan2 bisa berkunjung langsung dan ketemu dengan Pak Joe..🔥
</p>
                        <div class="testi-img_block">
                            <!--<img src="<?php echo base_url() .
                                                'theme/images/student-2.png'; ?>" class="img-fluid" alt="#">-->
                            <p><span><b>~ Arif Saefudin ~</b></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--============================= END =============================-->
