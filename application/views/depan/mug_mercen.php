<head>
    <title><?php echo $title; ?></title>
</head>

<!--============================= SLIDER =============================-->
<section class="content">

      <!-- Default box -->
      <div class="card card-solid" style="margin: 20px;">
        <div class="card-body" >
          <div class="row">

            <div class="col-12 col-sm-6">
              <div class=" col-lg-12 mt-3 text-center" style="height: max-content;">
                <div class="wrimagecard wrimagecard-topimage">
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?php echo base_url() . 'theme/images/jualan/mug.png' ?>" class="d-block w-100" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>

              <div class="col-12 col-sm-4" style="margin: 5px;">
                <h3 class="my-3"><b>Mug Serba Guna [ Official Rumah Scopus ]</b></h3>
                <p>Cocok untuk anda ngopi atau ngeteh. Official Rumah Scopus.</p>
                <hr>
                <div class="bg-gray py-2 px-3 mt-4">
                  <h2 class="mb-0b">
                    Rp.145.000,00-
                  </h2>
                </div>

                <div class="mt-4">
                  <a href="https://tokopedia.link/7cImJvlvQub">
                  <div class="btn btn-success btn-lg btn-flat">
                    <!--<i class="fas fa-cart-plus fa-lg mr-2"></i> -->
                    Tokopedia
                  </div>
                  </a>
                  <a href="https://shope.ee/3fTYoT7rSC">
                  <div class="btn btn-lg btn-flat" style="background-color: orange; color:white;">
                    <!--<i class="fas fa-cart-plus fa-lg mr-2"></i> -->
                    Shopee
                  </div>
                  </a>
                </div>

              

              </div>
            </div>
            <div class="row mt-4">
              <nav class="w-100">
              <div class="nav nav-tabs" id="product-tab" role="tablist" style="float: left; margin:20px;">
                <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc" role="tab" aria-controls="product-desc" aria-selected="true">Description</a>
              </div>
            </nav>
            <div class="tab-content p-3" id="nav-tabContent" style="margin: 5px;;">
              <div class="tab-pane fade show active" id="product-desc" role="tabpanel" aria-labelledby="product-desc-tab"> 
              Mug ini cocok untuk anda ngeteh atau ngopi di teras rumah. Mug ini Official Dari Rumah Scopus.
            </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
<!--//END WELCOME TITLE --> 