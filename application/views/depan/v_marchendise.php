<head>
    <title><?php echo $title; ?></title>
</head>

<!--============================= SLIDER =============================-->
<section>
    <div class="slider_img layout_two">
        <div id="carousel" class="carousel slide" data-ride="carousel">
           
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img  class="d-block" src="<?php echo base_url() .'theme/images/jualan/bannernovember.png'; ?>" alt="First slide">
                </div>
            </div>
        </div>
    </div>
</section>
<!--============================= END =============================-->
<!--============================= WELCOME TITLE =============================-->
<section class="welcome_about">
    <div class="container ">
        <div class="row">
            <div class="col-md-12 mb-5">
                
                <center>
                    <h1>Marchendise Kami</h1>
                    <hr size="50%" width="25%" align="center" color="orange">
                </center>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-3 col-sm-6 col-lg-3 mb-5" style="height: max-content;">
                <div class="wrimagecard wrimagecard-topimage" style="border: 2px solid orange;">
                    <div class="card-header text-center" style="background-color: orange; color:white;">
                        <b>Best Seller!</b>
                    </div>
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?php echo base_url() . 'theme/images/jualan/buku1.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/buku2.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/buku3.png' ?>" class="d-block w-100" alt="...">
                            </div>
                        </div>
                    </div>
                    <h5 class="card-title mt-3 text-center" style="margin: 4px;"><b>Buku Cara Cerdik Menulis Manuskrip Paper Scopus</b></h5>
                    <a href="<?php echo site_url('buku'); ?>">
                    <center>
                        <button type="button" class="btn btn-info mt-4 mb-3" style="margin: 3px;">Pesan Sekarang!</button>
                    </center>
                </a>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-lg-3 mb-5">
                <div class="wrimagecard wrimagecard-topimage mt-5">
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?php echo base_url() . 'theme/images/jualan/tumbler1.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/tumbler2.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/tumbler3.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/tumbler4.png' ?>" class="d-block w-100" alt="...">
                            </div>
                        </div>
                    </div>
                    <h5 class="card-title mt-3 text-center" style="margin: 4px;"><b>Tumbler Serba Guna [Official Rumah Scopus]</b></h5>
                    <center>
                        <button type="button" class="btn btn-info mt-4 mb-3" style="margin: 3px;">Pesan Sekarang!</button>
                    </center>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-lg-3 mb-5">
                <div class="wrimagecard wrimagecard-topimage mt-5">
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?php echo base_url() . 'theme/images/jualan/mug1.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/mug2.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/mug3.png' ?>" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo base_url() . 'theme/images/jualan/mug4.png' ?>" class="d-block w-100" alt="...">
                            </div>
                        </div>
                    </div>
                    <h5 class="card-title mt-3 text-center" style="margin: 4px;"><b>Mug Serba Guna [Official Rumah Scopus]</b></h5>
                    <center>
                        <button type="button" class="btn btn-info mt-4 mb-3" style="margin: 3px;">Pesan Sekarang!</button>
                    </center>
                </div>
            </div>

        </div>
    </div>
</section>
<!--//END WELCOME TITLE -->